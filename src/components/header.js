import SwitchToDarkIcon from "../images/icon-moon.svg";
import SwitchToLightIcon from "../images/icon-sun.svg";

export const Header = ({ themeLight, setThemeLight }) => {
    const switchThemeIcon = themeLight ? SwitchToDarkIcon : SwitchToLightIcon;
  
    const changeTheme = () => {
      setThemeLight(!themeLight);
    };
  
    return(
        <header>
            <h1>TODO</h1>
            <button classname = "btn switch-theme-btn" onClick={changeTheme}>
                <img src={switchThemeIcon} alt="Dark Theme" />
            </button>
        </header>
    )

};